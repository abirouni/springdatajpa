package com.Springboot.Demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Springboot.Demo.entity.Employee;
import com.Springboot.Demo.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	public List<Employee> getemployee() {
		return employeeRepository.findAll();
	}

	public Employee getEmployeeById(int idEmployee) {
		Optional<Employee> result = employeeRepository.findById(idEmployee);
		Employee theEmployee = null;

		if (result.isPresent()) {
			theEmployee = result.get();
		} else {
			throw new RuntimeException("Did not find employee id -" + idEmployee);
		}
		return theEmployee;
	}

	public Employee addEmployee(Employee employee) {
		return null;
	}

	public Employee updateEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

	public void deleteEmployee(int idEmpolyee) {
		employeeRepository.deleteById(idEmpolyee);

	}

	public void saveEmployee(Employee employee) {
		employeeRepository.save(employee);

	}

}
