package com.Springboot.Demo.service;

import java.util.List;

import com.Springboot.Demo.entity.Employee;

public interface EmployeeService {

	public List<Employee> getemployee();

	public Employee getEmployeeById(int idEmployee);

	public Employee addEmployee(Employee employee);

	public Employee updateEmployee(Employee employee);

	public void deleteEmployee(int idEmpolyee);

	public void saveEmployee(Employee employee);

}
