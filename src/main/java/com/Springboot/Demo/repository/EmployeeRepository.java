package com.Springboot.Demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Springboot.Demo.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
